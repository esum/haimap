from enum import IntEnum as Enum
import queue
import random
import re
import select
import socket
import ssl
import sys
import threading


class InvalidUri(Exception):
	pass

class ImapAuthType(Enum):
	LOGIN = 0
	SASL = 1

class ImapAuth:

	def __init__(self, type, **kwargs):
		self.type = type
		if type == ImapAuthType.LOGIN:
			self.user_name = kwargs['user_name']
			self.password = kwargs['password']
		elif type == ImapAuthType.SASL:
			self.mechanism = kwargs['machanism']
		else:
			raise ValueError(type)

class ImapConnection:

	def __init__(self, uri, auth, box='INBOX', starttls=False, ssl_context=None, server_hostname=None, dump=False):
		self.uri = uri
		self.auth = auth
		self.box = box
		self.starttls = starttls
		if uri.startswith('imaps://') or starttls:
			if ssl_context is None:
				self.ssl_context = ssl.create_default_context()
			else:
				self.ssl_context = ssl_context
		else:
			self.ssl_context = None
		self.server_hostname = server_hostname
		self.dump = dump

	def connect(self):
		match = re.fullmatch(r'(?P<scheme>imaps?)://(?P<domain>[A-Za-z0-9_.-]+)(?::(?P<port>\d+))?', self.uri)
		if match is None:
			raise InvalidUri(self.uri)
		domain = match.group('domain')
		port = int(match.group('port')) if match.group('port') is not None else None
		scheme = match.group('scheme')
		if port is None:
			if scheme == 'imap':
				port = 143
			elif scheme == 'imaps':
				port = 993
			else:
				raise InvalidUri(self.uri)
		self.identifier = 'a001'
		self.socket_raw = socket.create_connection((domain, port))
		if scheme == 'imaps' or self.starttls:
			if self.starttls:
				raise NotImplementedError
			server_hostname = domain if self.server_hostname is None else self.server_hostname
			self.socket = self.ssl_context.wrap_socket(self.socket_raw, server_hostname=server_hostname)
		else:
			self.socket = self.socket_raw
		if self.auth.type == ImapAuthType.LOGIN:
			self.login(self.auth.user_name, self.auth.password)
		self.select(self.box)

	def increment_identifier(self):
		alpha = self.identifier[:-3]
		numeric = int(self.identifier[-3:])
		if numeric < 999:
			numeric += 1
			self.identifier = f'{alpha}{numeric:03}'
		else:
			numeric = 1
			alpha = 'a'
			self.identifier = f'{alpha}{numeric:03}'

	def _get_command_response(self, socket=None, bufsize=4096, identifier=None, blocking=True, partial=False):
		if socket is None:
			socket = self.socket
		if identifier is None:
			identifier = self.identifier
		response = b''
		response_received = False
		while not response_received:
			if blocking:
				data = socket.recv(bufsize)
			else:
				try:
					data = socket.recv(4096)
				except ssl.SSLWantReadError:
					return
			if not data:
				# socket is closed
				return []
			if self.dump:
				for line in data.split(b'\r\n'):
					print(self, self.uri, '<', line)
			response += data
			if len(response) >= 2 and response[-1] == 10 and response[-2] == 13:
				if partial:
					return response.split(b'\r\n')[:-1]
				else:
					last_line = response.rsplit(b'\r\n', 2)[-2]
					if last_line.startswith(f'{identifier} '.encode('ascii')):
						response_received = True
		return response.split(b'\r\n')[:-1]

	def login(self, user_name, password):
		self.socket.sendall(f'{self.identifier} LOGIN {user_name} {password}\r\n'.encode('ascii'))
		self._get_command_response()
		self.increment_identifier()

	def select(self, box):
		self.socket.sendall(f'{self.identifier} SELECT {box}\r\n'.encode('ascii'))
		self._get_command_response()
		self.increment_identifier()

	def idle(self):
		self.socket.sendall(f'{self.identifier} IDLE\r\n'.encode('ascii'))
		data = self.socket.read(4096)
		self.socket.setblocking(0)

	def done(self):
		self.socket.setblocking(1)
		self.socket.sendall(f'DONE\r\n'.encode('ascii'))
		responses = self._get_command_response()
		if responses is None:
			return []
		message_ids = []
		for response in responses:
			if response.endswith(b' EXISTS'):
				message_ids.append(int(response[2:-len(b' EXISTS')].decode('ascii')))
		self.increment_identifier()
		return message_ids

	def fetch(self, message):
		self.socket.sendall(f'{self.identifier} FETCH {message} (RFC822)\r\n'.encode('ascii'))
		responses = self._get_command_response()
		mail = b''
		size = None
		for response in responses:
			if size is not None:
				mail += response
				mail += b'\r\n'
				size -= len(response) + 2
				if size <= 0:
					if size < 0:
						print(self, self.uri, f"Read too many bytes for message {message}", file=sys.stderr)
					break
			elif response.startswith(f'* {message} FETCH '.encode('ascii')):
				match = re.fullmatch(r'.*\{(?P<size>\d+)\}', response.decode('ascii'))
				size = int(match.group('size'))
		self.increment_identifier()
		return mail

	def capability(self, raw=False):
		if raw:
			socket = self.socket_raw
		else:
			socket = self.socket
		socket.sendall(f'{self.identifier} CAPABILITY\r\n'.encode('ascii'))
		response = self._get_command_response(socket)
		capabilities = []
		for command in response:
			if command.startswith(b'* CAPABILITY '):
				capabilities.extend(capability.decode('ascii') for capability in command[len(b'* CAPABILITY '):].split())
		self.increment_identifier()
		return capabilities

class Imap:

	def __init__(self, connections, message_callback, initial_context=None, epoll_timeout=None):
		self.epoll = select.epoll()
		self.fd_to_connection = {}
		self.queue = queue.Queue()
		if initial_context is None:
			context = {}
		else:
			context = initial_context
		connect_thread = threading.Thread(target=self.connect, args=(connections,))
		connect_thread.start()
		while True:
			message_ids = set()
			register = {}
			done = set()
			for fd, event in self.epoll.poll(epoll_timeout):
				if event & (select.EPOLLERR | select.EPOLLHUP):
					server = self.fd_to_connection.pop(fd)
					self.epoll.unregister(fd)
					self.queue.put(server)
				elif event & select.EPOLLIN:
					connection = self.fd_to_connection[fd]
					self.epoll.unregister(fd)
					connection_message_ids = set()
					responses = connection._get_command_response(blocking=False, partial=True)
					while responses:
						for response in responses:
							if response.endswith(b' EXISTS'):
								connection_message_ids.add(int(response[2:-len(b' EXISTS')].decode('ascii')))
						responses = connection._get_command_response(blocking=False, partial=True)
					message_ids |= connection_message_ids
					if connection_message_ids:
						done.add(connection)
						for message_id in connection.done():
							message_ids.add(message_id)
					register[fd] = connection
				else:
					print(fd, event)
			for message_id in message_ids:
				connection = random.choice(list(register.values()))
				message = connection.fetch(message_id)
				threading.Thread(target=message_callback, args=(message, context)).start()
			for fd, connection in register.items():
				if connection in done:
					connection.idle()
				self.epoll.register(fd, select.EPOLLIN | select.EPOLLERR | select.EPOLLHUP)

	def connect(self, connections):
		offline_servers = [connection for connection in connections]
		while True:
			for server in offline_servers:
				try:
					server.connect()
				except OSError:
					offline_servers.append(server)
					print(server.uri, "appears to be offline", file=sys.stderr)
				except TimeoutError:
					offline_servers.append(server)
					print(server.uri, "appears to be offline", file=sys.stderr)
				except Exception as exn:
					print(server.uri, exn, file=sys.stderr)
				else:
					server.idle()
					offline_servers.remove(server)
					fd = server.socket.fileno()
					self.fd_to_connection[fd] = server
					self.epoll.register(fd, select.EPOLLIN | select.EPOLLERR | select.EPOLLHUP)
			if not offline_servers:
				offline_servers.append(self.queue.get())
